#!/usr/bin/env python3
import sys
import numpy as np
from pprint import pprint
import os


class TextAnalyzer:
    def __init__(self, text):
        self.text = text
        self.punctuation = [',', '.', ':', ';', '-', '#', '@', '&', '?', '!', "/n"]

    def calc_ngrams(self, n):
        words = self._clean_(self.text).split()
        ngrams = {}
        for i in range(len(words) - n):
            ngrams[' '.join(words[i:i+n])] = ngrams.get(' '.join(words[i:i+n]), 0) + 1
        return ngrams

    def get_word_counts(self):
        unigrams = {}
        sentences = self.text.split('.')
        for sentence in sentences:
            words = self._clean_(sentence).split()
            for word in words:
                unigrams[word] = unigrams.get(word, 0) + 1
        return unigrams

    def mean_sentence_word_count(self):
        words_count = 0
        sentences = self.text.split('.')
        for sentence in sentences:
            words_count += len(self._clean_(sentence).split())
        return words_count / len(sentences)

    def median_sentence_word_count(self):
        words_counts = []
        sentences = self.text.split('.')
        for sentence in sentences:
            words_counts.append(len(self._clean_(sentence).split()))
        return np.median(words_counts)

    def top_ngrams(self, n=4, k=10):
        words = self._clean_(self.text).split()
        ngrams = {}
        for i in range(len(words) - n):
            ngrams[' '.join(words[i:i+n])] = ngrams.get(' '.join(words[i:i+n]), 0) + 1
        i = 0
        top_grams = {}
        for key in sorted(ngrams, key=ngrams.__getitem__, reverse=True):
            if i >= k:
                break
            top_grams[key] = ngrams[key]
            i += 1
        return top_grams

    def _clean_(self, text):
        clean_text = text.strip()
        for sym in self.punctuation:
            clean_text = clean_text.replace(sym, '')
        return clean_text.lower()


def print_all(text, n=1, k=5):
    if not text or not n or not k:
        print('whoops')
        return
    analyzer = TextAnalyzer(text)
    unigrams = analyzer.get_word_counts()
    print('Word counts:')
    pprint(unigrams)
    top_ngrams = analyzer.top_ngrams(n=n, k=k)
    mean_wc = analyzer.mean_sentence_word_count()
    median_wc = analyzer.median_sentence_word_count()
    print('Top {k} {n}-grams:'.format(n=n, k=k))
    pprint(top_ngrams)
    print('Mean word count: {wc}, Median word count: {mc}'.format(wc=mean_wc, mc=median_wc))


def main(input_file):
    if os.path.exists(input_file):
        with open(input_file, 'r', encoding='utf-8') as f:
            text = f.read().replace('\n', '')
            print_all(text)


if __name__ == '__main__':
    main(sys.argv[1])
