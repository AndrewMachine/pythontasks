import random


def qs(a, l, r):
    mid = (l + r) // 2
    i = l
    j = r
    while i <= j:
        while a[i] < a[mid]:
            i += 1
        while a[j] > a[mid]:
            j -= 1
        if i <= j:
            a[i], a[j] = a[j], a[i]
            i += 1
            j -= 1
    if i < r:
        a = qs(a, i, r)
    if j > l:
        a = qs(a, l, j)
    return a


def quick_sort(a):
    return qs(a, 0, len(a) - 1)


def merge(a, b):
    c = []
    len_a = len(a)
    len_b = len(b)
    i = 0
    j = 0
    while i < len_a and j < len_b:
        if a[i] < b[j]:
            c.append(a[i])
            i += 1
        else:
            c.append(b[j])
            j += 1
    while i < len(a):
        c.append(a[i])
        i += 1
    while j < len(b):
        c.append(b[j])
        j += 1
    return c


def merge_sort(a):
    if len(a) > 1:
        mid = len(a) // 2
        left = merge_sort(a[:mid])
        right = merge_sort(a[mid:])
        return merge(left, right)
    return a


def radix_sort(a):
    radix = 10
    max_length = False
    tmp, placement = -1, 1
    while not max_length:
        max_length = True
        buckets = [list() for _ in range(radix)]
        for num in a:
            tmp = num // placement
            buckets[tmp % radix].append(num)
            if max_length and tmp > 0:
                max_length = False
        i = 0
        for b in range(radix):
            bucket = buckets[b]
            for j in bucket:
                a[i] = j
                i += 1
        # move to next digit
        placement *= radix
    return a


def main():
    nums = list(map(int, input().split()))
    #nums = quick_sort(nums, 0, len(nums) - 1)
    #print(nums)
    #nums = list(random.shuffle(nums))
    #nums = merge_sort(nums)
    nums = radix_sort(nums)
    print(nums)

if __name__ == '__main__':
    main()