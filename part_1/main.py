import fibonacci
import interactive_dict
import sorts
import text_metricks
import argparse


def text(args):
    try:
        with open(args.file, 'r', encoding='utf-8') as f:
            text_metricks.print_all(f.read().replace('\n', ''), args.n, args.k)
    except Exception as e:
        print('something went wrong:', e)
        return


def sort(args):
    try:
        with open(args.file, 'r', encoding='utf-8') as f:
            arr = list(
                    map(int,
                        f.read().replace('\n', '').strip().split()
                        )
                    )
            if args.radix:
                arr = sorts.radix_sort(arr)
            elif args.merge:
                arr = sorts.merge_sort(arr)
            else:
                arr = sorts.quick_sort(arr)
            print('Sorted array:')
            print(*arr)
    except Exception as e:
        print('something went wrong:', e)
        return


def fib(args):
    try:
        with open(args.file, 'r', encoding='utf-8') as f:
            fibonacci.run(int(f.read().replace('\n', '').strip()))
    except Exception as e:
        print('something went wrong:', e)
        return


def interactive_set(args):
    interactive_dict.run()


def set_parsers():
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(help='sub-command help')

    text_parser = subparsers.add_parser('text', help='text metricks subprogram')
    text_parser.add_argument("file", type=str, help="set input file name")
    text_parser.add_argument("n", type=int, help="set ngram length")
    text_parser.add_argument("k", type=int, help="set amount of top ngrams")
    text_parser.set_defaults(func=text)

    sorts_parser = subparsers.add_parser('sorts', help='sorts subprogram')
    sorts_parser.add_argument("file", type=str, help="set input file name")
    group = sorts_parser.add_mutually_exclusive_group()
    group.add_argument("-q", "--quick", action="store_true", help="sort with quick sort")
    group.add_argument("-m", "--merge", action="store_true", help="sort with merge sort")
    group.add_argument("-r", "--radix", action="store_true", help="sort with radix sort")
    sorts_parser.set_defaults(func=sort)

    fibonacci_parser = subparsers.add_parser('fib', help='fibonacci subprogram')
    fibonacci_parser.add_argument("file", type=str, help="set input file name")
    fibonacci_parser.set_defaults(func=fib)

    set_parser = subparsers.add_parser('set', help="""interactive set subprogram; \n
                                                      add k1 [, k2, ...] - add key in set, \n
                                                      remove k1 [, k2, ...] - remove key if exists, \n
                                                      list - print all elements""")
    set_parser.set_defaults(func=interactive_set)

    args = parser.parse_args()
    args.func(args)


def main():
    set_parsers()

if __name__ == '__main__':
    main()
