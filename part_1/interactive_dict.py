import re


class MySet:
    def __init__(self, set=set()):
        self.data = set

    def add(self, keys):
        for key in keys:
            self.data.add(key)

    def remove(self, keys):
        for key in keys:
            self.data.discard(key)

    def find(self, keys):
        existing_keys = []
        for key in keys:
            if key in self.data:
                existing_keys.append(key)
        return existing_keys

    def list(self):
        return ' '.join(self.data)

    def grep(self, regex):
        match = []
        pattern = re.compile(regex)
        for key in self.data:
            if pattern.match(key):
                match.append(key)
        return match

    def save(self, filename):
        with open(filename, 'w') as f:
            f.write(self.list())

    def load(self, filename):
        self.data = set()
        try:
            with open(filename, 'r') as f:
                nums = f.readline().strip().split()
                self.add(nums)
        except FileNotFoundError:
            print('File not found')
        except ValueError:
            print('Wrong file format')


def run():
    my_set = MySet()
    while True:
        s = input()
        args = s.split()
        command = args[0].strip().lower()
        keys = args[1:]
        if command == 'add':
            my_set.add(keys)
        elif command == 'remove':
            my_set.remove(keys)
        elif command == 'find':
            print(*my_set.find(keys))
        elif command == 'list':
            print(my_set.list())
        elif command == 'grep':
            print(*my_set.grep(keys[0]))
        elif command == 'load':
            my_set.load(keys[0])
        elif command == 'save':
            my_set.save(keys[0])
        elif command == 'exit':
            break


def main():
    run()

if __name__ == '__main__':
    main()