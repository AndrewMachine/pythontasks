import sys
import argparse


def fibonacci(last):
    a = 1
    b = 1
    while a < last:
        yield a
        a, b = b, a + b


def run(n):
    generator = fibonacci(n)
    a = 1
    while a:
        try:
            a = next(generator)
            print(a)
        except StopIteration:
            break


def main():
    run(1000)

if __name__ == '__main__':
    main()
