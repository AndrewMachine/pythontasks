import re


def main():
    print('Enter string to check: ')
    s = input()
    print('1) Check email')
    print('2) Check float number')
    print('3) Get url parts')
    c = input()
    if c == '1':
        email_template = re.compile('[\w#$%]+(\.[-\w+#$%_]+)*@[a-zA-Z]+\.[a-z]+')
        if not email_template.match(s):
            print('kek')
        else:
            print('match')
    elif c == '2':
        float_template = re.compile('^[+-]?([\d]*\.?[\d]*|[\d]+)$')
        if not float_template.match(s):
            print('kek')
        else:
            print('match')

if __name__ == '__main__':
    main()
