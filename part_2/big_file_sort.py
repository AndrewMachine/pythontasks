import argparse
import sys
import tempfile
import os
import io


def sort(buffer, comparator=None, reverse=False):
    if comparator:
        sorted_buffer = sorted(
            buffer,
            key=comparator,
            reverse=reverse)
    else:
        sorted_buffer = sorted(buffer, reverse=reverse)
    return sorted_buffer


def big_string_comparator(s, field_separator, positions):
    parts1 = s.strip().split(field_separator)
    n = len(parts1)
    good_positions = [pos for pos in positions if pos < n]
    s_prepared = ''.join([parts1[i] for i in range(n) if i in good_positions])
    return s_prepared


def is_bigger(s1, s2):
    n = min(len(s1), len(s2))
    for i in range(n):
        if s1[i] > s2[i]:
            return True
        elif s1[i] < s2[i]:
            return False
    if len(s1) > len(s2):
        return False
    return True


def merge(buffer, output_file, reverse=False):
    with tempfile.TemporaryFile(mode='w+') as f1, open(output_file, 'a+', encoding='utf-8') as out, \
            tempfile.TemporaryFile(mode='a+') as f2:
        f1.write('\n'.join(buffer)+'\n')
        f1.seek(0)
        out.seek(0)
        a = f1.readline()
        b = out.readline()
        if b == '':
            b = out.readline()
        while a != '' and b != '':
            res = is_bigger(a.strip().replace('\n', ''), b.strip().replace('\n', ''))
            if reverse:
                res = not res
            while a != '' and b != '' and not res:
                if a != '\n':
                    f2.write(a)
                a = f1.readline()
                res = is_bigger(a.strip().replace('\n', ''), b.strip().replace('\n', ''))
                if reverse:
                    res = not res
            while a != '' and b != '' and res:
                if b != '\n':
                    f2.write(b)
                b = out.readline()
                res = is_bigger(a.strip().replace('\n', ''), b.strip().replace('\n', ''))
                if reverse:
                    res = not res
        while a != '' and a != '\n':
            f2.write(a)
            a = f1.readline()
        if a != '' and a != '\n':
            f2.write(a)
        while b != '' and b != '\n':
            f2.write(b)
            b = out.readline()
        if b != '' and b != '\n':
            f2.write(b)
        f2.seek(0)
        # print('----------')
        # print(f2.readlines())
        # print('----------')
        # f2.seek(0)
        out.close()
        with open(output_file, 'w', encoding='utf-8') as out:
            for line in f2:
                out.write(line)


def get_line(input_file=None):
    if not input_file:
        s = input() + '\n'
    else:
        s = input_file.readline()
    return s


def sort_big_file(input_name='input.txt', output_name='output.txt', is_from_file=False,
                  max_buffer_size=1000, is_numeric=False, line_separator='\n',
                  field_separator='\t', reverse=False, positions=None):
    f = open(output_name, 'w')
    f.close()
    s = 'a'
    cur_buffer_size = 0
    buffer = []
    syms = []

    input_file = None
    if is_from_file:
        input_file = open(input_name, 'r', encoding='utf-8')
    while True:
        s = get_line(input_file)
        if s == '':
            break
        stop = False
        for sym in s:
            if sym == '\n' and line_separator != '\n':
                continue
            if sym != line_separator:
                syms += [sym]
            else:
                s = ''.join(syms)
                syms = []
                if s == '':
                    continue
                s_size = sys.getsizeof(s)
                if s_size > max_buffer_size:
                    stop = True
                    break
                if cur_buffer_size + s_size <= max_buffer_size:
                    buffer.append(s)
                    cur_buffer_size += s_size
                else:
                    buffer = sort(buffer, reverse=reverse,
                                  comparator=lambda x: big_string_comparator(x, field_separator, positions))
                    merge(buffer, output_name, reverse=reverse)
                    buffer = [s]
                    cur_buffer_size = s_size
        if stop:
            break
    if len(syms) or cur_buffer_size:
        s = ''.join(syms)
        buffer.append(s)
        buffer = sort(buffer, reverse=reverse, comparator=lambda s: big_string_comparator(s, field_separator, positions))
        merge(buffer, output_name, reverse=reverse)
    if is_from_file:
        input_file.close()


def check_order(input_name=None, reverse=False, line_separator='\n'):
    s = 'a'
    cur_buffer_size = 0
    syms = []
    prev = ''
    input_file = None
    if input_name:
        input_file = open(input_name, 'r', encoding='utf-8')
    while True:
        s = get_line(input_file)
        if s == '':
            break
        stop = False
        for sym in s:
            if sym == '\n' and line_separator != '\n':
                continue
            if sym != line_separator:
                syms += [sym]
            else:
                s = ''.join(syms)
                syms = []
                if s == '':
                    continue
                s_size = sys.getsizeof(s)
                if s_size > max_buffer_size:
                    stop = True
                    break


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-n', '--numeric', help='Consider that substrings are numbers',
                        action='store_true', default=False)
    parser.add_argument('--reverse', action='store_true', help='Sort from max to min', default=False)
    parser.add_argument('--check', help='Check order', action='store_true', default=False)
    parser.add_argument('--input', help='Input file name', default='input.txt')
    parser.add_argument('--output', help='Input file name', default='output.txt')
    parser.add_argument('--line_separator', help='Symbol that separates lines', default='\n')
    parser.add_argument('--field_separator', help='Symbol that separates parts of line', default='\t')
    parser.add_argument('--buffer_size', help='Size of the buffer', default=10000)
    parser.add_argument('-k', '--key', help='Keys to sort', nargs='+', default=None)

    args = parser.parse_args()
    positions = None
    if args.key:
        positions = list(map(int, args.key))
    return args.line_separator, args.field_separator, args.buffer_size, \
           args.check, args.reverse, args.input, args.output, positions


def main():
    is_numeric = False
    is_from_file = True
    line_separator, field_separator, max_buffer_size, check, reverse, input_name, output_name, positions = parse_args()
    sort_big_file(input_name, output_name, is_from_file, max_buffer_size, positions=positions,
                  reverse=reverse, line_separator=line_separator, field_separator=field_separator)


if __name__ == '__main__':
    main()
